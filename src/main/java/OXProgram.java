
import java.util.Scanner;

public class OXProgram {

    static String player = "X";
    static String table[][] = new String[3][3];
    static int row, col, round;
    static Scanner kb = new Scanner(System.in);
    static String rowCheck, colCheck, crossCheck1, crossCheck2;
    static boolean winner = false;

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
        System.out.println(" 1 2 3");
        System.out.println("1- - -");
        System.out.println("2- - -");
        System.out.println("3- - -");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        int num_row = 0;
        for (int i = 0; i < 3; i++) {
            num_row++;
            System.out.print(num_row);
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        System.out.println("Please input Row Col: ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInput();
    }

    static void checkInput() {
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            input();
        } else {
            checkTableEmpty();
        }
    }

    static void checkTableEmpty() {
        if (table[row][col].equals("-")) {
            table[row][col] = player;
        } else {
            input();
        }
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkCross1();
        checkCross2();
        checkDraw();
    }

    static void checkRow() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                rowCheck = rowCheck + table[i][j];
            }
            if (rowCheck.equals("XXX") || rowCheck.equals("OOO")) {
                winner = true;
                break;
            }
            rowCheck = "";
        }
    }

    static void checkCol() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                colCheck = colCheck + table[j][i];
            }
            if (colCheck.equals("XXX") || colCheck.equals("OOO")) {
                winner = true;
                break;
            }
            colCheck = "";
        }
    }

    static void checkCross1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                crossCheck1 = crossCheck1 + table[j][j];
            }
            if (crossCheck1.equals("XXX") || crossCheck1.equals("OOO")) {
                winner = true;
                break;
            }
            crossCheck1 = "";
        }
    }

    static void checkCross2() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0, m = 2; j < 3; j++, m--) {
                crossCheck2 = crossCheck2 + table[j][m];
            }
            if (crossCheck2.equals("XXX") || crossCheck2.equals("OOO")) {
                winner = true;
                break;
            }
            crossCheck2 = "";
        }
    }

    static void checkDraw() {
        if (round == 8) {
            System.out.println("Draw ...");
        }
    }

    static void switchPlayer() {
        if (player.equals("X")) {
            player = "O";
        } else {
            player = "X";
        }
    }

    static void isFinish() {
        if (winner == true) {
            showResult();
            round = 9;
        }
    }

    static void showResult() {
        System.out.println("Player " + player + " win ...");
    }

    static void showBye() {
        System.out.println("Bye bye ...");
    }

    public static void main(String[] args) {
        //put '-' in the table
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
        showWelcome();

        //start game
        for (round = 0; round < 9; round++) {
            showTurn();
            input();
            showTable();
            checkWin();
            isFinish();
            switchPlayer();
        }
        showBye();
    }
}
